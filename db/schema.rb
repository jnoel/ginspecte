# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_10_055356) do
  create_table "histories", force: :cascade do |t|
    t.datetime "date"
    t.integer "service_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "duration"
    t.boolean "ping"
    t.integer "block"
    t.integer "error"
    t.string "version"
    t.integer "nb_documents"
    t.index ["service_id"], name: "index_histories_on_service_id"
  end

  create_table "service_types", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.string "message"
    t.string "picto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string "url"
    t.boolean "status"
    t.integer "uptime"
    t.string "ipv4"
    t.string "ipv6"
    t.string "admin"
    t.string "server_funding"
    t.integer "service_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "port", default: 443
    t.string "version"
    t.boolean "ssl", default: true
    t.integer "last_nb_documents"
    t.integer "last_uptime"
    t.integer "last_error"
    t.index ["service_type_id"], name: "index_services_on_service_type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "histories", "services"
  add_foreign_key "services", "service_types"
end
