class AddVersionToService < ActiveRecord::Migration[7.0]
  def change
    add_column :services, :version, :string
  end
end
