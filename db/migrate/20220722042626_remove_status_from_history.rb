class RemoveStatusFromHistory < ActiveRecord::Migration[7.0]
  def change
    remove_column(:histories, :status)
  end
end
