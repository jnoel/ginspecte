class AddSslToService < ActiveRecord::Migration[7.0]
  def change
    add_column :services, :ssl, :boolean, default: true
  end
end
