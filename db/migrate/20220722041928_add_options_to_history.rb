class AddOptionsToHistory < ActiveRecord::Migration[7.0]
  def change
    add_column :histories, :duration, :integer
    add_column :histories, :ping, :boolean
    add_column :histories, :block, :integer
    add_column :histories, :comment, :string

    History.all.each do |history|
      status = YAML.load(history.status)
      history.ping = status["result"]
      history.block = status["number"]
      history.save
    end
  end
end
