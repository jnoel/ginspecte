class AddTypes < ActiveRecord::Migration[7.0]
  def change
    ServiceType.create name: 'Duniter', picto: 'people', position: 1
    ServiceType.create name: 'Cesiumplus', picto: 'people', position: 2
    ServiceType.create name: 'WotWizard', picto: 'people', position: 3
  end
end
