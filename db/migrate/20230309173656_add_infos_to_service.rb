class AddInfosToService < ActiveRecord::Migration[7.0]
  def change
    add_column :services, :last_nb_documents, :integer
    add_column :services, :last_uptime, :integer
  end
end
