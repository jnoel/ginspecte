class AddLastErrorToService < ActiveRecord::Migration[7.0]
  def change
    add_column :services, :last_error, :integer
  end
end
