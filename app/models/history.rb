class History < ApplicationRecord
  belongs_to :service

  def define_error(option)
    return define_error_duniter(option) if self.service.service_type.name == 'Duniter'
    return self.error
  end

  def define_error_duniter(last_block)
    if self.block.to_i == 0
      self.error = 1
    else
      late = self.block.to_i < last_block-10
      if last_block && !late
        self.error = 0
        self.ping = true
      elsif last_block
        self.error = 2
        self.ping = true
      else
        self.error = 1
        self.ping = false
      end
    end
    self.save
    return self.error
  end

  def error_string
    string = case self.error
    when 0
      'status_ok'
    when 1
      'status_down'
    when 2
      'status_late'
    when nil
      nil
    end
    return string
  end

end
