class Service < ApplicationRecord
  belongs_to :service_type
  has_many :histories, :dependent => :delete_all

  def uptime
    histories = self.histories.last(30)
    nb_ping = histories.count
    positive_ping = (histories.select{|history| history.ping == true}).count

    return nb_ping.positive? ? ((positive_ping*100)/nb_ping).to_i : 0
  end

  def ping
    case service_type.name
    when 'Duniter'
      self.ping_duniter
    when 'Cesiumplus'
      self.ping_cesiumplus
    when 'WotWizard'
      self.ping_wotwizard
    else
      self.ping_other
    end
  end

  def get_full_url
    full_url = "#{self.url}"
    slash_index = self.url.index('/')
    slash_index ? full_url.insert(slash_index, ":#{self.port}") : full_url.concat(":#{self.port}")
    full_url.insert(0, self.ssl ? 'https://' : 'http://')
  end

  def ping_duniter
    number = nil
    duration = nil
    full_url = get_full_url
    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{full_url}/blockchain/current", :timeout => 10, :open_timeout => 10)
      duration = (requete.duration * 1000).to_i
      number = JSON.parse(requete.body)["number"]
    rescue
      puts "Impossible de joindre l'API Duniter pour #{full_url}"
    end

    begin
      requete = RestClient::Request.execute(:method => :get, :url => full_url, :timeout => 10, :open_timeout => 10)
      self.version = JSON.parse(requete.body).first[1]["version"]
      self.save
    rescue
      puts "Impossible de récupérer la version Duniter pour #{self.url}"
    end

    History.create(date: DateTime.now, service_id: self.id, block: number, duration: duration)
  end

  def ping_cesiumplus
    duration = nil
    nb_documents = 0

    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}/node/summary", :timeout => 10, :open_timeout => 10)
      self.version = JSON.parse(requete.body).first[1]["version"]
      #self.status = true
    rescue
      #self.status = false
      puts "Impossible de récupérer la version Cesium+ pour #{self.url}"
    end

    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}/node/stats", :timeout => 10, :open_timeout => 10)
      duration = (requete.duration * 1000).to_i
      nb_documents = requete.split('docs').last.split('count')[1].split(':')[1].split(',').first.to_i
      self.status = nb_documents.positive?
    rescue
      self.status = false
      puts "Impossible de joindre l'API Cesium+ pour #{self.url}"
    end
    self.save

    History.create(date: DateTime.now, service_id: self.id, ping: self.status, nb_documents: nb_documents, duration: duration, error: self.status ? 0 : 1)
  end

  def ping_wotwizard
    begin
      client = SimpleGraphqlClient::Client.new(url: "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}")
      version = client.query(gql: %{query {version}}).version
      self.status = !version.empty?
      self.version = version
    rescue
      self.status = false
    end
    self.save

    History.create(date: DateTime.now, service_id: self.id, ping: self.status, error: self.status ? 0 : 1)
  end

  def ping_other
    ping = nil
    begin
      ping = Net::Ping::TCP.new(self.url, self.port).ping?
    rescue
      puts "Erreur de test pour #{self.url}"
      ping = false
    end
    self.status = ping
    self.save

    History.create(date: DateTime.now, service_id: self.id, ping: ping, error: self.status ? 0 : 1)
  end


  def last_block
    return nil unless self.type?('Duniter')

    self.histories.last&.block
  end

  def type?(type_name)
    self.service_type.name == type_name
  end

  def status_classe
    case self.last_error
    when 0
      return 'ok'
    when 1
      return 'ko'
    when 2
      return 'soso'
    else
      return 'nil'
    end
  end

  def check_service(cb=nil)
    cb = self.service_type.current_block if cb.nil?
    history = self.histories.last
    if history
      error = history.define_error(cb)
      self.status = (error==0)
      self.last_error = error
      self.last_nb_documents = history.nb_documents.to_i | history.block.to_i
    end
    self.last_uptime = self.uptime
    self.save
  end

  def nb_documents
    self.histories.last.nb_documents
  end
  
  def error_string
    string = case self.last_error
    when 0
      'status_ok'
    when 1
      'status_down'
    when 2
      'status_late'
    when nil
      nil
    end
    return string
  end
end

