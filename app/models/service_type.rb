class ServiceType < ApplicationRecord
    has_many :services, :dependent => :delete_all

    def ping
        self.services.each { |service| service.ping } 
    end

    def current_block
        return nil unless self.name == 'Duniter'

        blocks = []
        self.services.each do |service| 
            block = service.last_block
            blocks << block if block
        end
        return blocks.max.to_i
    end

    def check_services_block
        cb = self.current_block
        self.services.each do |service|
            service.check_service(cb)
        end
    end

end
