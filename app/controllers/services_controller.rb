class ServicesController < ApplicationController
  before_action :set_service, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, only: %i[ edit update destroy ]

  # GET /services or /services.json
  def index
    @services = Service.all
  end

  # GET /services/1 or /services/1.json
  def show
    @histories = @service.histories.last(30).reverse
  end

  # GET /services/new
  def new
    @service = Service.new
    @service.service_type_id = params[:service_type_id]
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services or /services.json
  def create
    @service = Service.new(service_params)

    respond_to do |format|
      if @service.save
        @service.ping
        @service.check_service
        format.html { redirect_to @service.service_type, notice: I18n.t('service_created') }
        format.json { render :show, status: :created, location: @service }
      else
        format.html { render :new, status: :unprocessable_entity, alert: I18n.t('service_no_created') }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1 or /services/1.json
  def update
    respond_to do |format|
      if @service.update(service_params)
        format.html { redirect_to service_url(@service), notice: 'service_updated' }
        format.json { render :show, status: :ok, location: @service }
      else
        format.html { render :edit, status: :unprocessable_entity, alert: 'service_no_updated' }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1 or /services/1.json
  def destroy
    service_type = @service.service_type_id
    @service.destroy

    respond_to do |format|
      format.html { redirect_to service_type_path(service_type), notice: I18n.t('service_destroyed'), status: :see_other }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def service_params
      params.require(:service).permit(:url, :port, :uptime, :ipv4, :ipv6, :admin, :server_funding, :service_type_id, :ssl)
    end
end
