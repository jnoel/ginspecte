json.extract! service_type, :id, :name, :position, :message, :picto, :created_at, :updated_at
json.url service_type_url(service_type, format: :json)
