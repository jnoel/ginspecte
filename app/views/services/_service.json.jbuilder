json.extract! service, :id, :url, :status, :uptime, :ipv4, :ipv6, :admin, :server_funding, :service_type_id, :created_at, :updated_at
json.url service_url(service, format: :json)
